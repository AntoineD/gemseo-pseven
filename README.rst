..
    Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com

    This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
    International License. To view a copy of this license, visit
    http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative
    Commons, PO Box 1866, Mountain View, CA 94042, USA.

pSeven wrapper for GEMSEO

WARNING: This plugin is no longer tested nor updated, contact us in case of trouble.

Documentation
-------------

See https://gemseo.readthedocs.io/en/stable/plugins.html.

Installation
~~~~~~~~~~~~

Using pSeven requires that is installed as well as its Python API.
The pSeven Python API is not defined as a dependency of this package,
because there is no package available in PyPI.
It shall be installed in the same environment as the one in which this plugin is installed.

Development
~~~~~~~~~~~

For testing with ``tox``,
set the environment variable :envvar:`PSEVEN_PIP_REQ_SPEC`
to point to the URL or path of a ``pip`` installable version of the pSeven Python API,
Set the environment variable :envvar:`DATADVD_LICENSE_FILE`
for the pSeven license.

Bugs/Questions
--------------

Please use the gitlab issue tracker at
https://gitlab.com/gemseo/dev/gemseo-pseven/-/issues
to submit bugs or questions.

License
-------

The GEMSEO-PSEVEN source code is distributed under the GNU LGPL v3.0 license.
A copy of it can be found in the LICENSE.txt file.
The GNU LGPL v3.0 license is an exception to the GNU GPL v3.0 license.
A copy of the GNU GPL v3.0 license can be found in the LICENSES folder.

The GEMSEO-PSEVEN examples are distributed under the BSD 0-Clause, a permissive
license that allows to copy paste the code of examples without preserving the
copyright mentions.

The GEMSEO-PSEVEN documentation is distributed under the CC BY-SA 4.0 license.

The GEMSEO-PSEVEN product depends on other software which have various licenses.
The list of dependencies with their licenses is given in the CREDITS.rst file.

Contributors
------------

- GEMSEO developers
